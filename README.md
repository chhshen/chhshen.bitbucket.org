

CSWEB: My personal website based on Markdown2Web and BibTeX2x
==========


This code is built on top of <https://bitbucket.org/chhshen/markdown2web>.
Tested on OSX 10.9 with [Homebrew](http://brew.sh/) installed.



## Requirements

A  recent version of Python and the following package
(only if you want to convert a BibTeX file into publication lists.
 Otherwise you don't need to install bibtex2x):

[bibtex2x](https://bitbucket.org/chhshen/bibtex2x)

Install using pip:

~~~
pip install git+https://bitbucket.org/chhshen/bibtex2x/
~~~

and [pandoc](http://johnmacfarlane.net/pandoc/), which can be downloaded
at [http://code.google.com/p/pandoc/downloads/](http://code.google.com/p/pandoc/downloads/list).

You may also need a GNU version of [sed](http://www.gnu.org/software/sed/).

## Start

To start, all you need to do is:

~~~
git clone https://bitbucket.org/chhshen/markdown2web
~~~

or git clone this repo.



and then modify the `config.yaml' file and replace the bibtex file in the directory with your own.
You can start writing your own markdown files to build your website.

Finally, just type

~~~
make
~~~

and see the directory ./htmls

To deploy, all you need to do is to scp or rsync ./htmls/* to your web server.

## Design your own navigation menu theme

You need to write your own *\_template/nav_theme?.html* and *\_assets/css/nav_theme?.css*.
Here ? is the index number (from 1 to 10).

