#
# makefile for csweb
# Chunhua Shen, Aug. 2013
#

ODIR=./www


.PHONY: clean

all:  dist tidy clean  tar scp


build: 
	./build.sh
	./tools/insert_link.sh index.html
		

# call html-tidy
tidy:
	./build.sh $(ODIR)

clean:
	rm -f *html
	rm -f publications/*html
	rm -f publications/paper*md
	rm -f publications/*bib
	rm -f publications/$(ODIR)/*


dist: build pdf_pub_list
	mkdir -p $(ODIR)
	rm -fr $(ODIR)/*
	cp -f cs.bib $(ODIR)
	cp -r _assets $(ODIR)
	for f in `ls -1 *.html *.pdf`; \
	do \
    	cp $$f   $(ODIR); \
	done 
	echo "."
	echo "The output files for your website are in: $(ODIR)."


tar: dist
	tar zcvf htmls.tar.gz  $(ODIR)


purge: clean
	rm -fr publications/year/
	rm -fr publications/output
	rm -f htmls.tar*
	rm -f cshen_*.pdf


pdf_pub_list:
	echo "generate paper list in a pdf file (it make take a minute):"
	./gen_pdf_list.sh         # &> /dev/null


scp:
	./scp2srv
#	cp -f ./tools/index_bitbucket_home.html   ./index.html


