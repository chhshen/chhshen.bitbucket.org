#!/usr/bin/env fish
# Chunhua Shen, Feb 2014
#
# This Fish script extract your uploaded arXiv papers' information, given the link of an arXiv link
#
# Format of the outputs:
#
# Line 1: paper link
#      2: title
#      3: abstract
#      4: authors
#      5: pdf link
# BLANK
# then paper 2's information, and so on
#
# http://arxiv.org/a/shen_c_1.atom2
# extract arxiv info from config.yaml


set arxiv (cat ../config.yaml | grep -i arxiv | grep -v \# | sed 's/:/  /' |  awk '{ print $2 }' | tr -d '[:space:]')

if [ (count $arxiv) -eq 0  ]
    echo "... No arxiv information found. Please set the arxiv string in ../config.yaml"
    exit 1
end

set  atom $arxiv.atom2

set  tmpf /tmp/(random).xml

# echo "atom link: "$atom
curl $atom -o $tmpf

# echo "done."


xmllint --format $tmpf > _t

mv -f _t $tmpf

set p (cat $tmpf)
set Prefix "    "

set abstract_print 0
echo "parsing, it may take a few minutes ..."

echo "" > _F
echo "" > _F_all


for i in (seq (count $p) )

    # skip the first a few lines
    if [ $i -lt 7 ]
            continue
    end

    set -l ll  (math "$i - $i / 100 * 100" )
    if test $ll = 0; and echo "processing text line: $i"; end

    # removing leadning spaces
    set pi (echo $p[$i] | sed 's/^ *//')
    set pi $Prefix$pi

    if echo $pi | grep \<entry\> > /dev/null

      if grep EXCLUDEME _F > /dev/null

      else
        cat _F  >> _F_all
      end

      echo "" > _F
    end

  begin

    if echo $pi | grep \<id\> > /dev/null
        set abstract_print 0
        echo " "
        echo $pi | sed -e s/\<id\>// | sed -e s/\<\\/id\>//
    end

    if echo $pi  | grep \<title\> > /dev/null; echo $pi  | sed -e s/\<title\>// | sed -e s/\<\\/title\>//; end
    if echo $pi  | grep \<name\> > /dev/null; echo $pi  | sed -e s/\<name\>// | sed -e s/\<\\/name\>//; end

    if echo $pi  | grep \<link | grep pdf > /dev/null
        echo -n $Prefix; and echo $pi  | awk '{ print $3  }' | sed -e s/href=// | sed -e s/\"//g
    end

    if echo $pi  | grep \<summary\> > /dev/null
        echo -n $Prefix
        set abstract_print 1
    end

    if  echo $pi  | grep \<\/summary\> > /dev/null
        echo $pi  | sed 's/^ *//' | sed -e s/\<summary\>// | sed -e s/\<\\/summary\>//
        set abstract_print 0
    end


    if [ $abstract_print -eq  1 ]
        echo -n $pi | sed 's/^ *//'  | sed -e s/\<summary\>// | sed -e s/\<\\/summary\>//
    end

    if echo $pi | grep '<arxiv:comment'  | grep --ignore-case withdrawn >/dev/null
        echo $Prefix"EXCLUDEME"
    end

 end >> _F

end




/bin/rm  -f $tmpf
/bin/rm  -f _F
set -e tmpf
set -e arxiv
set -e atom

#
