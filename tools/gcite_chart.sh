#!/bin/bash
#
# Author:        Chunhua Shen [http://cs.adelaide.edu.au/~chhshen/], The University of Adelaide
# Creation:      Wednesday 04/07/2012 13:45.
# Last Revision: Wednesday 04/07/2014 14:36.
#
# August 2014 Google updated the Scholar page which breaks this script

# extract google scholar from config.yaml
google_scholar=""
# google_scholar=`cat ../config.yaml | grep -i google_scholar | grep -v \# | sed 's/:/  /' |  awk '{ print $2 }' | tr -d '[:space:]'`

if [ $google_scholar"" == "" ]
then
    echo "... No google scholar information found. Please set the google_scholar string in ../config.yaml"
    exit 1
fi



echo "... extracting citation chart from google scholar ..."

wget -q -O _t   "$google_scholar"



sed -n -e 's/.*src=\(.*\).*/\1/p' _t | awk '{print $1}' | sed -e 's/amp;//g' > _chart


Largest_citation=`cat _chart | sed 's/&chd/ /g' | awk '{ print $1  }' | sed 's/chxr=/ /g' | awk '{ print $2 }' \
           | awk -F',' '{ print $4 }'`

cat _chart | sed 's/chd=t:/  /g' | awk '{ print $2 }' | sed 's/&chxl=0:/   /g' > _ycite
year_start=`cat _ycite | sed 's/|/  /g' | awk '{ print $2 }'`
year_end=`cat _ycite | sed 's/|/  /g' | awk '{ print $NF }'  | sed 's/"//g'`

N=0
for i in `cat _ycite | awk '{ print $1 }' | sed 's/,/ /g'`
do
 let N+=1
 cite[ N ]=$( echo  "$Largest_citation  *  $i * 0.01" | bc -l )
done



echo "... your citations in recent years are:"
for i in `jot $N`
do
    echo ${cite[ $i ]}
done

echo "update the json file ..."
# {
#     "data": [[1999, 1], [2000, 0.23], [2001, 3], [2002, 4]]
# }

JFILE=google_scholar_cite.json
echo "{" >         $JFILE
echo '"data":[' >> $JFILE


for i in `jot $N`
do
    echo -n  "["$year_start", "${cite[ $i ]}"] "  >> $JFILE

    if [ "$i" -ne  "$N" ]
    then
        echo ","       >> $JFILE
    fi

    let year_start+=1
done

echo "]}"      >> $JFILE


DFILE=google_scholar_cite.dat
year_start=`cat _ycite | sed 's/|/  /g' | awk '{ print $2 }'`

echo "x y" >        $DFILE

for i in `jot $N`
do
    echo   $year_start" "${cite[ $i ]}  >> $DFILE
    let year_start+=1
done


# Generate PDF plot
[[ -d ./temp1_gcite ]] && rm -fr ./temp1_gcite
mkdir -p ./temp1_gcite
cp *dat  ./temp1_gcite
cp *tex  ./temp1_gcite
cd       ./temp1_gcite


cat google_scholar_pgfplots_template.tex  | sed s/RETRDATE/"`date +%m.%Y`"/g > _t
mv -f _t google_scholar_pgfplots_template.tex

pdflatex google_scholar_pgfplots_template.tex  > /dev/null
pdflatex google_scholar_pgfplots_template.tex  > /dev/null

convert  -density 350 -trim google_scholar_pgfplots_template.pdf  -quality 100 google_scholar_cite.png

mv -f google_scholar_pgfplots_template.pdf   ../google_scholar_cite.pdf
mv -f google_scholar_cite.png ..


cd ..
[[ -d ./temp1_gcite ]] && rm -fr ./temp1_gcite




# mv -f $JFILE ../data/
rm -f _chart _ycite
rm -f google_scholar_cite.dat
rm -f _t


