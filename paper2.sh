#!/bin/bash

BIBTEX_FILE=`cat config.yaml | grep bibtex: | awk '{  print $2 } ' `

bibclean  $BIBTEX_FILE  -no-warnings  -max-width 2000   \
                         --delete-empty-values > cs1.bib




index=$(cat config.yaml | grep paper_theme_index | awk  '{ print $2 }' | tr -d '[:space:]')

if test x$index != x'2'; then

  exit 2

fi





INFILE=$1

while IFS= read -r line
do
    case "$line" in
        *INCLUDExxxBIBTEXxxxHERE)
            cat  cs1.bib
            ;;
        *)
            echo "$line"
            ;;
    esac
done  <"$INFILE"

exit 0

