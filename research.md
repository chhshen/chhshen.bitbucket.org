# Research

We are working on Deep Learning with applications to Large-scale image/video analysis.





## Grants

My recent research is mainly funded by the [Australian Research Council (ARC)](http://www.arc.gov.au)
  and a few
  industrial partners:
  [BAE](http://www.baesystems.com.au),
  [NICTA](http://www.nicta.com.au),
  [LBT Innovations](http://www.lbtinnovations.com),
  Defence Systems Innovation Centre.

