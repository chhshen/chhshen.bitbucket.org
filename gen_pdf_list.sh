#!/bin/bash
#
# Author:        Chunhua Shen [http://cs.adelaide.edu.au/~chhshen/], The University of Adelaide
# Creation:      Tuesday 08/01/2013 16:29.
# Last Revision: Saturday 12/01/2013 19:44.


#
# You need to have GNU version of sed
#

function shorten( )
{
    INPUT_FILE=$1

    cat $INPUT_FILE | gsed 's/Journal \+of/J.\\/gI' | \
                      gsed 's/Conference \+on/Conf.\\/gI'  | \
                      gsed 's/International/Int\x27l\\/gI' | \
                      gsed 's/Transactions \+On/Trans.\\/gI' >    _tmp

    mv -f _tmp  $INPUT_FILE
}



#
# [ ! -f  ocg-p.sty ] && wget http://mirrors.ctan.org/macros/latex/contrib/ocg-p/ocg-p.sty
#

cd list_pdf

cd bin
rm -f cs.bib

cp -f ../../cs.bib ./cs.bib



./bibparse.py  ./cs.bib
./num_total.sh


for f in `ls -1 *text | grep -v num`
do
    shorten $f
done


cd ..



TEXFILE="cshen_papers.tex"
OPT='--enable-pipes --shell-escape'


for i in $TEXFILE
do
    xelatex  $OPT $i
done



rm -fr *log *aux *out *exitCode *stderr

# rm -f bin/*.text


rm -f ../cshen_papers.pdf
cp -f cshen_papers.pdf  ../
rm -f ./cshen_papers.pdf
# rm bin/cs.bib

