


Selected  journal papers
=======================


pdf|<a href="#">year</a>|<a href="#">title</a>|<a href="#">venue</a>|<a href="#">authors</a>
-----|-----|-----|-----|-----:
{% for t in type -%}
{%  if t|lower == 'article'  and   venue[loop.index-1] |lower in  selected_venues | lower | str2list  -%}
{#-
 -#}
{% set HREF="#" -%}
{% set TTL="Journal paper. Click the paper title to see more" -%}
{% set ICON="fa-file-o fa-fw" -%}
{% if  pdf[loop.index-1]  -%}
   {%    set HREF= pdf[loop.index-1]    -%}
   {%    set TTL='Journal paper. Click to access the PDF file.' -%}
   {%    set ICON="fa-file-pdf-o fa-fw" -%}
{% endif  -%}
{% if  eprint[loop.index-1]  -%}
   {%    set HREF= [ 'http://arxiv.org/abs/', eprint[loop.index-1]  ] | join -%}
   {%    set TTL='Journal paper. Click to access the PDF file.' -%}
   {%    set ICON="fa-file-pdf-o fa-fw" -%}
{% endif  -%}
{#-
 -#}
<a data-toggle="tooltip" data-placement="left" title='{{ TTL }}' href='{{ HREF }}'><i class="fa {{ ICON }}"></i></a>| {{ year[loop.index-1]|replace('20','&rsquo;') }} |  <em><a href="{{ year[loop.index-1] }}.html#{{ key[loop.index-1] }}"> {{ title[loop.index-1] |   replace('{','')|replace('}','') }}  </a></em> {% if  project[loop.index-1]  %}<a href="{{ project[loop.index-1] }}" data-toggle="tooltip" data-placement="top" title='Project page' target="_blank" class="proj_more">[...more]</a>{% endif %} |<a data-toggle="tooltip" data-placement="top" title="{{ journal[loop.index-1] }}" href="#">{{  venue[loop.index-1] }}</a>| {{  authors[loop.index-1]|replace('{','')|replace('}','')  }}
{% endif  -%}
{% endfor -%}


