#!/usr/bin/env bash



[[ -z $1 ]] && echo "please input a bibtex file. abort." && exit -1

selected_venues=""
function check_selected_venues()
{
    selected_venues=`cat ../config.yaml | grep -i select_venues | grep -v \# | tr ':'  ' '  |  gawk '{ print $2 }' | \
         tr -d '[:space:]'`

    if [[ x$selected_venues == 'x' ]]
    then
        echo "no selected venues."
        return -1
    else
        return 0
    fi
}

# we need to check if a paper markdown file has no entries
# an empty one doesn't contain a string 'html'
function check_empty_markdown()
{
    f=$1
    cat $f | grep -i -q html
    if [[ ! $? == 0 ]]
    then
        echo "$f doesn't have any entries. cleaned!"
        rm -f $f
        # touch $f
    fi
}


google_scholar=""
google_scholar=`cat ../config.yaml | grep -i google_scholar | grep -v \# | gsed 's/:/  /' |  gawk '{ print $2 }' | tr -d '[:space:]'`

arxiv_entry=""
arxiv_entry=`cat ../config.yaml | grep -i arxiv_entry | grep -v \# | gsed 's/:/  /' |  gawk '{ print $2 }' | tr -d '[:space:]'`


function print_paper_menu()
{
cat <<HEADMSG
<br/>
<a href="#" data-xdrop="#x2"><em>Jump to </em> <b>↓</b></a>
<div id="x2" class="xdrop xdrop-tip xdrop-anchor-right">
<ul class="xdrop-menu">
HEADMSG
 [[ -f paper_all.md ]] && echo '<li><a href="paper_all.html"><i class="fa fa-file-code-o fa-fw"></i> full list • all </a></li>'
 [[ -f paper_journal.md ]] && echo '<li><a href="paper_journal.html"><i class="fa fa-file-code-o fa-fw"></i> full list • journal</a></li>'
 [[ -f paper_conf.md ]] && echo '<li><a href="paper_conf.html"><i class="fa fa-file-code-o fa-fw"></i> full list • conference</a></li>'
 [[ -f paper_misc.md ]] && echo '<li><a href="paper_misc.html"><i class="fa fa-file-code-o fa-fw"></i> full list • others</a></li>'
 echo '<li class="xdrop-divider"></li>'
 [[ -f paper_all_select.md ]] && echo '<li><a href="paper_all_select.html"><i class="fa fa-file-code-o fa-fw"></i> selected list • all</a></li>'
 [[ -f paper_journal_select.md ]] && echo '<li><a href="paper_journal_select.html"><i class="fa fa-file-code-o fa-fw"></i> selected list • journal</a></li>'
 [[ -f paper_conf_select.md ]] && echo '<li><a href="paper_conf_select.html"><i class="fa fa-file-code-o fa-fw"></i> selected list • conference</a></li>'
 [[ -f paper_misc_select.md ]] && echo '<li><a href="paper_misc_select.html"><i class="fa fa-file-code-o fa-fw"></i> selected list • others</a></li>'

# Add a PDF full paper list
    echo '<li class="xdrop-divider"></li>'
    echo -n '<li><a href="'
    echo -n 'cshen_papers.pdf"'
    echo -n '  target="_blank"'
    echo -n '><i class="fa fa-file-pdf-o fa-fw"></i> full list in pdf</a></li>'


 if [[ $google_scholar != ""  ]]; then
    echo '<li class="xdrop-divider"></li>'
    echo -n '<li><a href="'
    echo -n $google_scholar\"
    echo -n '  target="_blank"'
    echo -n '><i class="fa fa-google fa-fw"></i> Google scholar</a></li>'

 fi

 if [[ $arxiv_entry != ""  ]]; then
    echo '<li class="xdrop-divider"></li>'
    echo -n '<li><a href="'
    echo -n $arxiv_entry\"
    echo -n '  target="_blank"'
    echo -n '><i class="fa fa-google fa-fw"></i> arXiv</a></li>'

 fi

 echo '</ul></div>'
}

function paper_menu_infile()
{
    tmpf=/tmp/_process_bibtex_
    print_paper_menu > $tmpf
    cat $1 >> $tmpf

    mv -f $tmpf $1
}



tmp=bibtex_tidy.bib

bibtex2x -i $1 -o $tmp

num_journal=`grep -i @article $tmp  | wc -l | tr -d '[:space:]'`
num_conf=`grep -i @inproceedings $tmp  | wc -l | tr -d '[:space:]'`
num_misc=`grep -i @ $tmp | grep -v -i @inproceedings  | grep -v -i @article | wc -l | tr -d '[:space:]'`

# find years in the bibtex
years=`cat $tmp | egrep year  | gawk '{ print $3 }' | sort | uniq | tr  \" ' ' | tr , ' '`

bibtex2x -i $1  -o paper_journal.md -t journal_template.markdown --variable num_papers=$num_journal
bibtex2x -i $1  -o paper_conf.md    -t conf_template.markdown    --variable num_papers=$num_conf
bibtex2x -i $1  -o paper_misc.md    -t misc_template.markdown    --variable num_papers=$num_misc

check_empty_markdown paper_journal.md
check_empty_markdown paper_conf.md
check_empty_markdown paper_misc.md

rm -f paper_all.md
for f in  paper_journal.md paper_conf.md paper_misc.md; do
    [[ -f $f ]] && cat $f >> paper_all.md
done

#
# generate a list published in selected venues
#
check_selected_venues;
if [[ $? == 0 ]]
then
    bibtex2x -i $1  -o paper_journal_select.md -t sel_journal_template.markdown --variable selected_venues=$selected_venues
    bibtex2x -i $1  -o paper_conf_select.md    -t sel_conf_template.markdown    --variable selected_venues=$selected_venues
    bibtex2x -i $1  -o paper_misc_select.md    -t sel_misc_template.markdown    --variable selected_venues=$selected_venues
fi

check_empty_markdown paper_journal_select.md
check_empty_markdown paper_conf_select.md
check_empty_markdown paper_misc_select.md

rm -f paper_all_select.md
for f in  paper_journal_select.md paper_conf_select.md paper_misc_select.md; do
    [[ -f $f ]] && cat $f >> paper_all_select.md
done


for f in `ls -1 paper*md`; do
    paper_menu_infile  $f
done

mv  -f  paper*.md    ..


#
# split into years
#
[[ ! -d ./year ]] && mkdir ./year
for y in $years
do
   [[ ! -f year/$y.bib ]] && bibtex2x -i  $1  -o year/$y.bib -t year_template.bib \
                    --variable select_year=$y
done

_years_menu=''
for y in $years
do
    _years_menu=`echo $_years_menu $y`
done
years_menu=$( echo $_years_menu | tr ' ' , )



for y in `ls -1 year/*bib`
do
    # chk bib file size
    size=`cat $y | wc -c | tr -d '[:space:]'`
    num_papers=`grep -i @ $y  | wc -l | tr -d '[:space:]'`

    out_html_file=year/`basename $y .bib`.html

    [[ ! -f  $out_html_file  ]] && [[  $size -gt 20  ]]  &&          \
       bibtex2x  -i $y  -o  $out_html_file  -t  year_template.html \
        --variable num_papers=$num_papers                          \
        --variable years_menu="$years_menu"
done


