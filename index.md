# Chunhua Shen

I am a  Professor
of Computer Science at University of Adelaide, leading the [Adelaide Machine Learning Group](http://blogs.adelaide.edu.au/machine-learning/).


I held an [ARC Future Fellowship](http://www.arc.gov.au/future-fellowships)
from 2012 to 2016.
My research and teaching have been focusing on [Statistical Machine Learning](http://en.wikipedia.org/wiki/Machine_learning)
and [Computer Vision](http://en.wikipedia.org/wiki/Computer_vision).

These days my team spend most effort on [Deep Learning](https://en.wikipedia.org/wiki/Deep_learning). In
particular,  with tools from deep learning,
my research contributes to  understand the visual world around us by exploiting the large amounts of imaging data.



I received a PhD degree at [University of Adelaide](http://www.adelaide.edu.au); then
 worked at the [NICTA (formerly National ICT Australia)](http://www.nicta.com.au) computer vision program for about six years.
From 2006 to 2011, I held an adjunct position at College of Engineering & Computer Science,
[Australian National University](http://www.anu.edu.au).
I moved back to University of Adelaide in 2011.




<i class="fa fa-dot-circle-o"></i> News:


* Adelaide team is No. <b> ① </b> on [Cityscapes semantic segmentation](https://www.cityscapes-dataset.com/benchmarks/#scene-labeling-task).
  See our [paper](https://arxiv.org/abs/1611.10080).


* Adelaide team attended [ImageNet Challenge 2016 on the task of  Scene Parsing](http://image-net.org/challenges/LSVRC/2016/results)
and won the 2nd place.


* As of  March 2016,
  we achieved No. <b> ① </b> on ICDAR 2015 Challenge 2 “Focused Scene Text” for the task of End-to-End Scene
  Text Recognition. See the [Leaderboard](http://rrc.cvc.uab.es/?ch=2&com=evaluation).
  Details can be found in the [technical report](http://arxiv.org/abs/1601.05610).

* We have now designed new methods for semantic pixel labelling, which set the new record, and are 10 times faster!
Details can be found in our [technical report](http://arxiv.org/abs/1604.04339). See the [PASCAL VOC Leaderboard.](http://host.robots.ox.ac.uk:8080/leaderboard/displaylb.php?challengeid=11&compid=6)

* Adelaide vision team achieved No. <b> ① </b> for the task of semantic pixel labelling on PASCAL VOC 2012 (as of Feb., and Dec. 2016). See
[PASCAL VOC Leaderboard](http://host.robots.ox.ac.uk:8080/leaderboard/displaylb.php?challengeid=11&compid=6);
and the relevant papers: [CVPR’16](http://cs.adelaide.edu.au/~chhshen/2016.html#CVPR16labelling), and
[NIPS’15](http://cs.adelaide.edu.au/~chhshen/2015.html#NIPS15Lin).


* Adelaide team achieved No. <b> ④ </b> on the ImageNet Large Scale Visual Recognition Challenge 2015 (Object detection with provided training data).
See the [leaderboard](http://image-net.org/challenges/LSVRC/2015/results). The top 3 are  Microsoft research, Qualcomm research and SenseTime.


* Professor Shen is a Project Leader ([machine learning for robotic vision](http://roboticvision.org/what-we-do/learning-visual-learning-vl/vl2-learning-for-robotic-vision/))
and Chief Investigator (one of 13 <abbr title="Chief investigators">CIs</abbr>)
at the Australian  Centre  for Robotic Vision (ACRV),
which receives  a total of
$20 million in federal funding between 2014 and 2020.
He was also involved in the Data to Decisions CRC Centre (D2DCRC),
in particular on the projects of large scale image classification and text analysis.


<i class="fa fa-map-marker"></i> Places having been affiliated with:

* [University of Adelaide](http://www.adelaide.edu.au)
* [Australian  Centre  for Robotic Vision](http://roboticvision.org)
* [Data to Decisions Cooperative Research Centre](http://www.d2dcrc.com.au)
* NICTA, Canberra Research Laboratory
* [Bionic Vision Australia](http://bionicvision.org.au) (BVA)
* Australian National University, College of Engineering & Computer Science



<div id="Collapsible_bio">
<a href="#bio" data-toggle="collapse">
    <i class="fa  fa-arrow-circle-o-down fa-fw"></i> A formal, brief biography</a>
</div>

<blockquote id="bio" class="collapse">

<span lang="en" style="float:left;width:75%">

Chunhua Shen is a Professor at School of Computer Science, University of Adelaide.
He is a Project Leader and Chief Investigator at the Australian Research Council Centre of Excellence for Robotic Vision (ACRV),
for which he leads the project on machine learning for robotic vision.
Before he moved to Adelaide as a Senior Lecturer,
he was with the computer vision program at NICTA (National ICT Australia), Canberra Research
Laboratory for about six years. His research interests are in the
intersection of computer vision and statistical machine learning.
Recent work has been on large-scale image
retrieval and classification, object detection and pixel labelling using deep learning.
<br>
He studied at Nanjing University, at Australian National University,
and received his PhD degree from the University of Adelaide. From 2012
to 2016, he holds an Australian Research Council Future Fellowship.
He  served as Associate Editor of IEEE Transactions on Neural Networks and Learning Systems.
<br>
_Download photos_: [{2010}](_assets/Shen.jpg)[{2016}](_assets/Shen2.jpg)

<br>

</span>



<span lang="zh" style="float:left;width:75%">
**「中文简介」**
<br>
沈春华博士现任澳大利亚[阿德莱德大学](http://www.adelaide.edu.au/)[计算机科学学院](http://cs.adelaide.edu.au/)教授(终身教职)。
2011之前在[澳大利亚国家信息通讯技术研究院](http://www.nicta.com.au)堪培拉实验室的计算机视觉组工作近6年。
目前主要从事统计机器学习以及计算机视觉领域的研究工作。
主持多项科研课题，在重要国际学术期刊和会议发表论文100余篇。
2015,2016年担任IEEE Transactions on Neural Networks and Learning Systems
副主编。多次担任重要国际学术会议(ICCV, CVPR, ECCV等)程序委员。
<br>
他曾在南京大学(本科及硕士)，澳大利亚国立大学(硕士)学习，并在阿德莱德大学获得计算机视觉方向的博士学位。
2012年被澳大利亚研究理事会(Australian Research Council)授予Future Fellowship。
更多信息见: [cs.adelaide.edu.au/~chhshen/](http://cs.adelaide.edu.au/~chhshen/)

</span>

</blockquote>






# Group members

Supervision of PhD students and Postdoctoral researchers:


## Graduate students

--------------            -------------------------------------
Bohan Zhuang              [Adelaide](http://www.adelaide.edu.au)
Hui Li                    [Adelaide](http://www.adelaide.edu.au)
Tong Shen                 [Adelaide](http://www.adelaide.edu.au)
Tong He                   [Adelaide](http://www.adelaide.edu.au)
Ruoxi Deng                Visiting from Central South University, 2015---2017
Yu Chen                   Visiting from Nanjing University of Science and Technology, 2016---2017
Xiu-Shen Wei              Visiting from Nanjing University, 2016---2017
Hao Lu                    Visiting from Huazhong University of Science and Technology, 2016---2017
Ke Xian                   Visiting from Huazhong University of Science and Technology, 2016---2017
Huibing Wang              Visiting from Dalian University of Technology, 2016---2017
Liqian Liang              Visiting from Beijing Jiaotong University, 2016---2017
Pingping Zhang            Visiting from Dalian University of Technology, 2016---2017
Xueling Chen              Visiting from Northwestern Polytechnical University, 2016--2017
--------------            -------------------------------------



## Postdoctoral researchers

----------------------           -------------------------------------------------------
Dr. Chao Ma                      Project: Dense per-pixel prediction (ACRV)
Dr. Peng Wang (PhD UQ)           Project: Ultrasonic imaging analysis
Dr. Qi Wu                        Project: Image captioning and question answering (ACRV);  co-supervised with  Anton van den Hengel
Dr. Zifeng Wu                    Project: Large-scale image classification and object detection;  co-supervised with  Anton van den Hengel
----------------------           -------------------------------------------------------



## Graduated PhD, MPhil students

----------------               ---------  ------------------------
Yuanzhouhan Cao                2014-2017  Adelaide
Ruizhi Qiao                    2014-2017  Adelaide; &#x219D; Researcher at Tencent 腾讯优图
Qichang Hu                     2014-2017  Adelaide
Yao Li                         2013-2016  Adelaide (Dean’s Commendation for Doctoral Thesis Excellence); &#x219D; Researcher at Amazon Seattle
Teng Li                        2014-2016  Adelaide (Master by research)
Yuchao Jiang                   2015-2016  Adelaide (Master by research); &#x219D;  PhD student at UNSW
Junjie Zhang                   2014-2016  Adelaide (Master by research)
Fayao Liu                      2011-2015  Adelaide (Dean’s Commendation for Doctoral Thesis Excellence)
Guosheng Lin                   2011-2014  Adelaide (Dean’s Commendation for Doctoral Thesis Excellence, Google PhD Fellowship); &#x219D; Assistant Professor at Nanyang Technological University, Singapore
Lingqiao Liu                   2010-2013  ANU; co-supervised with  Lei Wang; &#x219D; ARC DECRA Fellow and Lecturer at University of Adelaide
Junae Kim                      2007-2011  ANU and NICTA;  &#x219D; Researcher at  DSTO
Hanxi Li                       2007-2011  ANU and NICTA;  &#x219D; Researcher at NICTA;  &#x219D; Professor at Jiangxi Normal University, China
Sakrapee Paul Paisitkriangkrai 2006-2010  UNSW and NICTA; &#x219D; Researcher at ACVT; &#x219D; Data Scientist at [ATO](https://www.ato.gov.au/)
Luping Zhou                    2006-2010  ANU and NICTA;  &#x219D; Senior Lecturer at [University of Wollongong](http://www.uow.edu.au)
----------------               ---------  ------------------------


## Departed PhD visitors

----------------               ---------  ------------------------
Yanzhu Zhao                    2016-2017  Visited from Zhejiang University of Technology
Xiao-Jiao Mao                  2015-2016  Visited from Nanjing University
Lei Zhang                      2015-2017  Visited from Northwestern Polytechnical University
Jiewei Cao                     2016         Visited from University of Queensland
Peng Wang                      2014-2016  Visited from University of Queensland
Zongyuan Ge                    2015         Visited from Queensland University of Technology; &#x219D; Researcher at IBM
Zetao Chen                     2015-2016  Visited from Queensland University of Technology; &#x219D; Researcher at ETH
Biyun Sheng                    2015-2016  Visited from Southeast University
Bo Li                          2013-2015  Visited from Northwestern Polytechnical University
Lei Luo                        2011-2013  Visited from National University of Defense Technology (NUDT); &#x219D; Lecturer at NUDT
Chao Zhang                     2011-2014  Visited from Beijing Institute of Technology; &#x219D; Researcher at Samsung Research
Zhihui Hao                     2010-2011  Visited from Beijing Institute of Technology;  &#x219D;  Research Engineer at    Baidu;  &#x219D; Research Engineer at  [Alibaba](http://www.alibabagroup.com/)
Fumin Shen                     2010-2012  Visited from Nanjing University of Science and Technology; &#x219D; Associate Professor at University of Electronic Science and Technology of China
Yongbin Zheng                  2009-2010  Visited from National University of Defense Technology (NUDT); &#x219D; Lecturer at NUDT
Peng Wang                      2008-2010  Visited from Beihang University; &#x219D; Researcher at ACVT
----------------               ---------  ------------------------


## Departed postdoctoral researchers

----------------                      ---------  ---------------
Dr. Lingqiao Liu                      2014-2016  Jointly supervised with Anton van den Hengel; &#x219D;  ARC DECRA Fellow and Lecturer at University of Adelaide
Dr. Peng Wang (PhD Beihang)           2013-2017  Jointly supervised with Anton van den Hengel; &#x219D; Professor at Northwestern Polytechnical University (入选青年千人计划)
Dr. Guosheng Lin                      2015-2016  &#x219D; Assistant Professor at Nanyang Technological University, Singapore
Dr. Sakrapee Paul Paisitkriangkrai    2010-2015  Jointly supervised with  Anton van den Hengel; &#x219D; Data Scientist at [ATO](https://www.ato.gov.au/)
Dr. Xi Li                             2011-2014  Co-supervised with  Anthony Dick; &#x219D; Professor at Zhejiang University (入选青年千人计划)
----------------                      ---------  ---------------


## Recruiting graduate students


<div class="container-fluid">
<div class="row-fluid">
<div class="span6" id="news_div">
We are actively recruiting PhD candidates. Applications should come through the
[University application system](http://www.adelaide.edu.au/graduatecentre/scholarships/postgrad/).
[Scholarships are available](http://www.adelaide.edu.au/graduatecentre/scholarships/postgrad/).
For overseas students, you need to have
[either TOEFL or IELTS results that meet the University's minimum requirement](http://www.adelaide.edu.au/study/postgraduate/research-degrees/requirements/english-language/).
</div>


<div class="span6" id="news_div">
For students from China:
University of Adelaide and the China Scholarship Council (CSC) have entered a
collaborative arrangement to provide research opportunities to select high
quality research students from China. Students selected as CSC scholarship
recipients will be able to enrol in a University of Adelaide PhD program and
undertake full-time study for up to four years.
</div>
</div>
</div>



# Contact


-----------------                                -------
<i class="fa fa-send fa-fw"></i>                 [School of Computer Science](http://cs.adelaide.edu.au), University of Adelaide, Adelaide, SA 5005, Australia
<i class="fa fa-university fa-fw"></i>           Level 5, Ingkarni Wardli Building (formerly Innova21 Building), University of Adelaide <a href="http://goo.gl/XuwMcT" data-toggle="tooltip" data-placement="right" title="Check the address on Google map"><i class="fa fa-map-marker"></i> Google map</a>
<i class="fa fa-fax fa-fw"></i>                  +61 (0)8 ~8303 4366
<i class="fa fa-phone-square fa-fw"></i>         +61 (0)8 ~8313 6745
<i class="fa fa-calendar  fa-fw"></i>            [Calendar](https://outlook.office365.com/owa/calendar/937271b24c194b92b7e0046c9b0b6f76@adelaide.edu.au/304daba0eb6045a1b05132c14f1217407951661187363938125/calendar.html)
<i class="fa fa-envelope-o fa-fw"></i>           chunhua.shen$@$adelaide.edu.au
<i class="fa fa-globe fa-fw"></i>                <http://cs.adelaide.edu.au/~chhshen/>
<i class="fa fa-skype fa-fw"></i>                chhshen$@$gmail.com
<i class="fa fa-bitbucket-square fa-fw"></i>     <https://bitbucket.org/chhshen/>
<i class="fa fa-github-square fa-fw"></i>        <https://github.com/chhshen/>
-----------------                                -------







