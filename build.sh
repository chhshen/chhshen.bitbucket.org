#!/bin/bash



# GNU sed, for osx, run `brew install gnu-sed'
GSed=gsed

function style_table()
{
    # replace <table> with
    # <table class="table table-striped table-condensed">
    temp=/tmp/_csweb_
    local ff="$1"
    cat $ff | $GSed 's/<table>/<table class=\"table table-striped table-condensed table-hover sortable\">/g' > $temp
    mv -f $temp $ff
}


function tidy_html()
{

    ! type tidy >/dev/null 2>&1 &&  echo "HTML tidy not found." && return -1


    #
    tidy --version | grep HTML5 > /dev/null
    if [ $? != 0  ]
    then
      echo "Error: "
      echo "       HTML Tidy for HTML5 (experimental) is needed. It looks that you are using an old version."
      echo "       If you are on OSX with homebrew, just install it: \`brew install --HEAD tidy'"
      return -2
    fi

    temp=/tmp/_csweb_
    local ff="$1"

    # replace & by XXXXX to avoid being replaced by html tidy
    cat $ff | $GSed 's/&/XXXXX/g'  >  $temp


    tidy -utf8 -q                                   \
         --drop-empty-elements false                \
         --wrap 0 --drop-proprietary-attributes yes \
    $temp > $ff

    # move back XXXXX to &
    cat $ff | $GSed 's/XXXXX/\&/g'  >  $temp

    mv -f $temp $ff
    echo "tidy html done: "$ff
}

#
# tidy all html files in a directory
#
function tidy_html_dir()
{

    if [[ -d $1 ]]
    then
        cd $1
    else
        echo "no html files found."
        exit 0
    fi

    for f in $( ls -1 *html  )
    do
        tidy_html $f
    done
}


# [[ ! -z $1 ]] && tidy_html_dir $1 && exit 0



#--------------------------------------
bibtex_file=""
function bibtexfile_fail()
{
    bibtex_file=$(cat config.yaml | grep -i bibtex | grep -v \# | tr ':'  ' ' | awk '{ print $2 }' | \
        tr -d '[:space:]')
    echo "the given bibtex file is: $bibtex_file"

    [[ ! -f $bibtex_file ]] && echo "no bibtex file; thus no publication list." && return 1
    echo "the bibtx file to process is: $bibtex_file"
    return 0
}

#
# check required software
#
! type pandoc >/dev/null 2>&1 &&  echo "$0 relies on pandoc. Abort." && exit -1

bibtexfile_fail;
if [[ $? == 0 ]]; then
#
# generate paper lists
#
echo "-------------------------------"
cd publications

    rm -f $bibtex_file
    ln -s ../$bibtex_file $bibtex_file
    ./process_bibtex.sh   $bibtex_file
cd ..
fi


for f in $( ls -1 *md | grep -v README )
do
     [[ -f  $( basename  $f .md).html ]]  && continue

     tmpfile=_$( basename  $f .md)_.html
     pandoc       $f  --mathjax  >  $tmpfile


     style_table  $tmpfile

     mv -f        $tmpfile  _content_.html

     python cs_mdweb/core.py

     rm -f        _content_.html
     mv -f        _output_.html    $( basename  $f .md).html
     echo         "converting markdown to html done: "$( basename  $f .md).html
done


[[ -f README.md  ]] && pandoc README.md -s > README.html









[[ -f paper2.sh ]] && ./paper2.sh  paper2.html  > _p.html  && mv -f _p.html paper2.html




# move paper_* to ./publications
for f in `ls -1 paper_*   2> /dev/null`
do
    mv -f  $f  ./publications
done



# process papers
[[ ! -d publications/output ]] && mkdir publications/output
for f in $( ls -1  publications/year | grep html )
do
     # skip if html exists.
     [[ -f publications/output/$f ]] && continue

     mv -f        publications/year/$f  ./_content_.html

     python       cs_mdweb/core.py
     rm -f        _content_.html
     mv -f        _output_.html    publications/output/$f
     echo         "converting  $f to html done:"

done


#
# make symblinks
#
for f in $( ls -1  publications/ | grep html )
do
    /bin/rm -f  $f
    echo "make symblink: $f"
    ln -s publications/$f  $f
done

for f in $( ls -1  publications/output/ | grep html )
do
    /bin/rm -f  $f
    echo "make symblink: $f"
    ln -s publications/output/$f  $f
done

if [[ -f paper_all_select.html ]];
then
    cp paper_all_select.html paper.html
else
    [[ -f paper_all.html ]] && cp paper_all.html paper.html
fi

