# -*- coding: utf8 -*-

# Copyright (c) 2014  Andrey Golovizin, Jorrit Wronski
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from pybtex.backends import BaseBackend
import pybtex.io


class Backend(BaseBackend):
    #TODO : Fix docs
    u"""
    >>> from pybtex.richtext import Text, Tag, Symbol
    >>> from pybtex.backends.markdown import Backend
    >>> testTag = Tag('emph', Text(u'Л.:', Symbol('nbsp'), u'<<Химия>>'))
    >>> print(testTag.render(Backend()))
    *Л.: <<Химия>>*

    >>> testTag = Tag('emph', Text(u'å:',u'{\\aa}'),Symbol(u'nbsp'),u'é:', u'\\\'e')
    >>> print(testTag.render(Backend(convert_latex=True)))
    *å:å é:é*

    """

    default_suffix = '.md'
    symbols = {
        'ndash': u'–',
        'newblock': u'  \n',
        'nbsp': u' '
    }
    tags = {
         'emph': u'*',
    }

    def format_str(self, text):
        """Can convert Latex to unicode.
        """
        if self.convert_latex:
            text = self.from_latex(text)
        if not self.encoding is None \
          and not self.encoding==pybtex.io.get_default_encoding():
            text = text.encode(self.encoding)
        return text

    def format_tag(self, tag_name, text):
        tag = self.tags[tag_name]
        return ur'%s%s%s' % (tag, text, tag)

    def format_href(self, url, text):
        return ur'![%s](%s)' % (text, url)

    def write_entry(self, key, label, text):
        # TODO: Decide for one version
        php_extra = False # Support http://www.michelf.com/projects/php-markdown/extra/#def-list
        if php_extra:
            self.output(u'%s\n' % label)
            self.output(u':   %s\n\n' % text)
        else:
            self.output(u'[%s] ' % label)
            self.output(u'%s  \n' % text)




