
from jinja2            import Environment, FileSystemLoader, PrefixLoader
from jinja2.exceptions import TemplateNotFound

import os

class Renderer(object):

    def __init__(self, path):
        self.environment = Environment(
                loader=FileSystemLoader( [path + os.sep + "_templates", path] )
                )

    def render( self, template, vars_ = {} ):
        try:

            template = self.environment.get_template(template)

        except TemplateNotFound:
            print( 'Template not found.' )
            raise

        return template.render(  **vars_  )

