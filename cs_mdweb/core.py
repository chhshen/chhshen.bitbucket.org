
from renderers import Renderer
from utils     import *
from fs        import Directory, File

import sys, yaml

class CsWeb(object):
    """
        Docstring for CsWeb
    """

    def __init__(self):
        """ @todo: to be defined1 """
        self.config_data = {}
        print('rendering ...')

    # Read the config file
    def _load_config(self):

        f = File(normpath(self.src.path, 'config.yaml'))
        if f.exists:
            self.config_data = yaml.load(f.content)
        else:
            print("missing config.yaml, abort.")
            sys.exit(1)


    # Render html
    def _render(self, variables):
        return Renderer(self.src.path).render("layout.html", variables)

    # Generate the html files
    def generate(self):
        self.src = Directory('.')

        self._load_config()

        vars = {}

        vars['latex']          = self.config_data['latex']
        vars['nav']            = self.config_data['nav']
        vars['nav_link_list']  = self.config_data['nav_link_list']
        vars['nav_title_list'] = self.config_data['nav_title_list']

        vars['title']          = self.config_data['title']
        vars['url']            = self.config_data['url']

        vars['nav_theme_index']   = self.config_data['nav_theme_index']
        vars['paper_theme_index'] = self.config_data['paper_theme_index']


        rendered = self._render(vars)

        # create index.html using rendered content
        out = File('_output_.html')
        if out.exists:
            out.rm()
        File('_output_.html', rendered).mk()




if __name__ == "__main__":
    CsWeb().generate()



