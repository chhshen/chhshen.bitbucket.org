# Notes


Code hosted at
[<i class="fa fa-bitbucket"></i> Bitbucket](https://bitbucket.org/chhshen/) and
[<i class="fa fa-github"></i> Github](https://github.com/chhshen/)


## Research Code

-----       ----------------------
 2015       [Depth estimation from monocular images using deep CRF](https://bitbucket.org/fayao/dcnf-fcsp)
 2015       [Mid-level pattern mining](https://github.com/yaoliUoA/MDPM)
 2014       [Pedestrian detection with spatially pooled features](https://github.com/chhshen/pedestrian-detection)
 2014       [Fast supervised hashing with decision trees for high-dimensional data](https://bitbucket.org/chhshen/fasthash/)
 2013       [A general two-step approach to learning-based hashing](https://bitbucket.org/guosheng/two-step-hashing/)
 2013       [Learning hash functions using column generation](https://bitbucket.org/guosheng/column-generation-hashing/)
 2013       [Contextual hypergraph modeling for salient object detection](https://bitbucket.org/chhshen/saliency-detection)
 2013       [Characterness: An indicator of text in the wild](https://bitbucket.org/chhshen/characterness-an-indicator-of-text-in-the-wild)
 2013       [Inductive hashing on manifolds](https://github.com/chhshen/Hashing-on-Nonlinear-Manifolds)
 2013       [A fast semidefinite approach to solving binary quadratic problems](http://code.google.com/p/boosting/downloads/detail?name=FastSDPCut_Release-1.0.zip)
 2013       [Incremental learning of 3D-DCT compact representations for robust visual tracking](https://github.com/chhshen/DCT-Tracking)
 2013       [A scalable stage-wise approach to large-margin multi-class loss based boosting](https://bitbucket.org/chhshen/a-scalable-stage-wise-approach-to-large-margin-multi-class)
 2013       [RandomBoost: Simplified multi-class boosting through randomization](http://code.google.com/p/boosting/downloads/detail?name=RandomBoost.zip)
 2013       [Part-based visual tracking with online latent structural learning](http://code.google.com/p/boosting/downloads/detail?name=PartTracker_v1.0.zip)
 2012       [Fast training of effective multi-class boosting using coordinate descent optimization](https://bitbucket.org/guosheng/fast-multiboost-cw)
 2012, 2009 [Positive semidefinite metric learning with boosting](http://code.google.com/p/boosting/downloads/detail?name=BoostMetric-NIPS09-codes-V0.1.tar.bz2)
 2010       [Generalized kernel-based visual tracking](http://code.google.com/p/detect/downloads/detail?name=generalized_kernel_tracking_C%2B%2B.tar.gz)
-----       ----------------------


## Other Code

------------                                                        ----------------------
[BibTeX2x](https://bitbucket.org/chhshen/bibtex2x)                  Converting a bibtex file into HTML, markdown, LaTeX, PDF etc.
[markdown2web](https://bitbucket.org/chhshen/markdown2web)          A simple solution to building lightweight websites
[handy script](https://bitbucket.org/chhshen/script)                Some handy scripts (bash, python, LaTeX etc.)
------------                                                        ----------------------

